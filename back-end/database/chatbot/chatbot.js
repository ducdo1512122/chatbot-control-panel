const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var chatbotSchema = new Schema({
    name: String,
    projectId: String // chatbotId
})


const chatbot = mongoose.model('chatbot', chatbotSchema, 'chatbot')

module.exports = chatbot