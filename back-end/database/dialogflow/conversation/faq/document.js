const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var documentSchema = new Schema({
    name: String,
    displayName: String,
    mimeType: String,
    knowledgeTypes: Array,
    rawContent: Buffer,
    knowledgeBaseId: String,
    documentId: String
})


const document = mongoose.model('document', documentSchema)

module.exports = document