const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var knowledgeBaseSchema = new Schema({
    name: String,
    displayName: String,
    languageCode: String,
    projectId: Array,
    knowledgeBaseId: String
})

const knowledgeBase = mongoose.model('knowledgeBase', knowledgeBaseSchema, "faqKnowledgeBase")

module.exports = knowledgeBase