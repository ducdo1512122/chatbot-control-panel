const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var pairsSchema = new Schema({
    question: String,
    answer: String,
    knowledgeBaseId: String,
    documentId: String
})

const pairs = mongoose.model('pairs', pairsSchema)

module.exports = pairs