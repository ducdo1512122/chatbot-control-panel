const express = require('express');
const router = express.Router();
const _chatbot = require("../../services/database/chatbot/chatbotDb")

// Live demo
router.get('/demo', function(req, res) {

})

// Get chatbot's info
router.get('/chatbot/show', function(req, res) {

})

// Create chatbot
router.post('/chatbot/new', function(req, res) {
    if (!req.body.name || !req.body.projectId) {
        return {
            status: 'fail',
            infomation: 'Check parameters!'
        }
    }

    _chatbot.createChatbot(req.body.name, req.body.projectId)
            .then(function(response) {
                res.send(response)
            })    
})

// Update chatbot' info
router.put('/chatbot/update', function(req, res) {

})

// Delete chatbot
router.delete('/chatbot/delete', function(req, res) {

})

// List all chatbots
router.get('/chatbot/showall', function(req, res) {
    _chatbot.listChatbots({}, {name: 1, projectId: 1, _id: 0})
            .then(function(response) {
                res.send(response)
            })
})

module.exports = router