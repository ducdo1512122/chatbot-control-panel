const express = require('express');
const router = express.Router();
const _knowledgeBase = require('../../services/dialoglow/_knowledgeBase/knowledgeBasesClient')
const _document = require('../../services/dialoglow/_knowledgeBase/documentsClient')
const _intent = require('../../services/dialoglow/_intents/intentsClient')
const _helper = require('../../utils/dialogflow/_helper')

// Get conversations
router.post('/faq/show', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _knowledgeBase.getKnowledgeBaseApi(req.body.projectId, req.body.knowledgeBaseId)
                    .then(function(response) {
                        res.send({
                            status: 'success',
                            information: response
                        })
                    })
                    .catch(function(err) {
                        res.send({
                            status: 'fail',
                            infomation: err
                        })
                    })
})

router.post('/dialog/show', function(req, res) {
    if (!req.body.projectId || !req.body.intentId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _intent.getIntentApi(req.body.projectId , req.body.intentId)
            .then(function(response) {
                res.send({
                    status: 'success',
                    information: response
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    infomation: err
                })
            })
})

// Create conversations
router.post('/faq/new', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseName) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }
 
    _knowledgeBase.createKnowledgeBaseApi(req.body.projectId, req.body.knowledgeBaseName)
                    .then(function(response) {
                        let returnedData = {}

                        returnedData.id = _helper.getIdFromPath(response.name) 
                        returnedData.displayName = response.displayName
                        returnedData.mode = "faq"

                        res.send({
                            status: 'success',
                            information: returnedData
                        })
                    })
                    .catch(function(err) {
                        res.send({
                            status: 'fail',
                            information: err
                        })
                    })
})

router.post('/faq/new/doc', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseId || !req.body.displayName || !req.body.content) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    let document = _document.documentContent(req.body.displayName, Buffer.from(req.body.content).toString('base64'))
    _document.createDocumentApi(req.body.projectId, req.body.knowledgeBaseId, document)
                .then(function(response) {
                    res.send({
                        status: 'success',
                        information: response
                    })
                })
                .catch(function(err) {
                    res.send({
                        status: 'fail',
                        information: err
                    })
                })
})

router.post('/dialog/new', function(req, res) {
    if (!req.body.projectId || !req.body.sessionId || !req.body.messages || !req.body.displayName || !req.body.trainingPhrases ||
            !req.body.inputContextNames || !req.body.outputContexts) {

        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    let inputContextNamesStructure = _intent.inputContextNamesStructure(req.body.projectId, req.body.sessionId, req.body.inputContextNames)
    let trainingPhrasesStructure = _intent.trainingPhrasesStructure(1, req.body.trainingPhrases)
    let outputContextsStructure = _intent.outputContextsStructure(req.body.projectId, req.body.sessionId, req.body.outputContexts, 5)
    let messagesStructure = _intent.messageStructure(req.body.messages)

    let intent = _intent.intentContent(null, null, req.body.displayName, null,
                                        null, null, inputContextNamesStructure,
                                        null, trainingPhrasesStructure, null,
                                        outputContextsStructure, null, null, messagesStructure)
        
    _intent.createIntentApi(req.body.projectId, intent)
            .then(function(response) {
                res.send({
                    status: 'success',
                    information: response
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    infomation: err
                })
            })

})

// Update conversation
router.put('/faq/update', function(req, res) {
    res.send({
        status: 'fail',
        information: 'Not supported'
    })
})

router.put('/faq/update/doc', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseId || !req.body.documentId ||
        !req.body.displayName || !req.body.content) {

        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    let document = _document.documentContent(req.body.displayName, Buffer.from(req.body.content).toString('base64'),
                        'text/csv', "FAQ", req.body.projectId, req.body.knowledgeBaseId, req.body.documentId)
    _document.updateDocumentApi(req.body.projectId, document)
                .then(function(response) {
                    res.send({
                        status: 'success',
                        information: response
                    })
                })
                .catch(function(err) {
                    res.send({
                        status: 'fail',
                        information: err
                    })
                })
})

router.put('/dialog/update', function(req, res) {
    if (!req.body.projectId || !req.body.sessionId || !req.body.intentId ||
            !req.body.messages || !req.body.displayName || !req.body.trainingPhrases ||
            !req.body.inputContextNames || !req.body.outputContexts) {

        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    let inputContextNamesStructure = _intent.inputContextNamesStructure(req.body.projectId, req.body.sessionId, req.body.inputContextNames)
    let trainingPhrasesStructure = _intent.trainingPhrasesStructure(1, req.body.trainingPhrases)
    let outputContextsStructure = _intent.outputContextsStructure(req.body.projectId, req.body.sessionId, req.body.outputContexts, 5)
    let messagesStructure = _intent.messageStructure(req.body.messages)

    let intent = _intent.intentContent(req.body.projectId, req.body.intentId, req.body.displayName, null,
                                        null, null, inputContextNamesStructure,
                                        null, trainingPhrasesStructure, null,
                                        outputContextsStructure, null, null, messagesStructure)
        
    _intent.updateIntentApi(req.body.projectId, intent)
            .then(function(response) {
                res.send({
                    status: 'success',
                    information: response
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    infomation: err
                })
            })
})

router.post('/dialog/batchUpdate', function(req, res) {
    if (!req.body.projectId || !req.body.sessionId || !req.body.batchIntents) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    let batchIntent = _intent.batchIntentContent(req.body.projectId, req.body.sessionId, req.body.batchIntents)
    _intent.batchUpdateIntentsApi(req.body.projectId, batchIntent)
            .then(function(response) {
                let returnedData = []

                for (let index = 0; index < response.intents.length; index++) {
                    let inputArr = []
                    for (let j = 0; j < response.intents[index].inputContextNames.length; j++) {
                        inputArr.push(_helper.getIdFromPath(response.intents[index].inputContextNames[j]))
                    }

                    let outputArr = []
                    for (let i = 0; i < response.intents[index].outputContexts.length; i++) {
                        outputArr.push(_helper.getIdFromPath(response.intents[index].outputContexts[i].name))
                    }

                    let trainingPhrasesArr = []
                    for (let k = 0; k < response.intents[index].trainingPhrases.length; k++) {
                        trainingPhrasesArr.push(response.intents[index].trainingPhrases[k].parts[0].text)                            
                    }

                    returnedData.push({
                        displayName: response.intents[index].displayName,
                        inputContextNames: inputArr,
                        trainingPhrases: trainingPhrasesArr,
                        outputContexts: outputArr,
                        messages: response.intents[index].messages[0].text.text, // message for now, payload later
                        id_: _helper.getIdFromPath(response.intents[index].name)
                    })  
                }

                res.send({
                    status: 'success',
                    information: returnedData
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    infomation: err
                })
            })

})

// Delete conversations
router.delete('/faq/delete', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _knowledgeBase.deleteKnowledgeBaseApi(req.body.projectId, req.body.knowledgeBaseId)
                    .then(function(response) {
                        res.send({
                            status: 'success',
                            information: response
                        })
                    })
                    .catch(function(err) {
                        res.send({
                            status: 'fail',
                            information: err
                        })
                    })
})

router.delete('/faq/delete/doc', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseId || !req.body.documentId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _document.deleteDocumentApi(req.body.projectId, req.body.knowledgeBaseId, req.body.documentId)
                .then(function(response) {
                    res.send({
                        status: 'success',
                        information: response
                    })
                })
                .catch(function(err) {
                    res.send({
                        status: 'fail',
                        information: err
                    })
                })
})

router.delete('/dialog/delete', function(req, res) {
    if (!req.body.projectId || !req.body.intentId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _intent.deleteIntentApi(req.body.projectId, req.body.intentId)
            .then(function(response) {
                res.send({
                    status: 'success',
                    information: response
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    information: err
                })
            })
})

router.delete('/dialog/batchDelete', function(req, res) {
    if (!req.body.projectId || !req.body.batchIntents) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    let deletedIntents = _intent.batchDeletedIntent(req.body.projectId, req.body.batchIntents)
    _intent.batchDeleteIntentsApi(req.body.projectId, deletedIntents)
            .then(function(response) {
                res.send({
                    status: 'success',
                    information: response
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    information: err
                })
            })
})

// Show all
router.post('/faq/showall', function(req, res) {
    if (!req.body.projectId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _knowledgeBase.listKnowledgeBasesApi(req.body.projectId)
                    .then(function(response) {
                        let returnedData = []

                        for (let index = 0; index < response.length; index++) {
                            returnedData.push({
                                    id: _helper.getIdFromPath(response[index].name),
                                    displayName: response[index].displayName,
                                    mode: 'faq'
                                })
                        }

                        res.send({
                            status: 'success',
                            information: returnedData
                        })
                    })
                    .catch(function(err) {
                        res.send({
                            status: 'fail',
                            information: err
                        })
                    })
})

router.post('/faq/showall/doc', function(req, res) {
    if (!req.body.projectId || !req.body.knowledgeBaseId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _document.listDocumentsApi(req.body.projectId, req.body.knowledgeBaseId)
                .then(function(response) {
                    let returnedData = []

                        for (let index = 0; index < response.length; index++) {
                            returnedData.push({
                                    id: _helper.getIdFromPath(response[index].name),
                                    displayName: response[index].displayName,
                                    content: _helper.buffer2string(response[index].rawContent),
                                    mode: 'faq'
                                })
                        }

                    res.send({
                        status: 'success',
                        information: returnedData
                    })
                })
                .catch(function(err) {
                    res.send({
                        status: 'fail',
                        information: err
                    })
                })
})

router.post('/dialog/showall', function(req, res) {
    if (!req.body.projectId) {
        return res.send({
            status: 'fail',
            infomation: 'Check parameters!'
        })
    }

    _intent.listIntentsApi(req.body.projectId)
            .then(function(response) {
                let returnedData = []

                for (let index = 0; index < response.length; index++) {
                    if (!response[index].displayName.startsWith("Knowledge.KnowledgeBase")) {

                        let inputArr = []
                        for (let j = 0; j < response[index].inputContextNames.length; j++) {
                            inputArr.push(_helper.getIdFromPath(response[index].inputContextNames[j]))
                        }

                        let outputArr = []
                        for (let i = 0; i < response[index].outputContexts.length; i++) {
                            outputArr.push(_helper.getIdFromPath(response[index].outputContexts[i].name))
                        }

                        let trainingPhrasesArr = []
                        for (let k = 0; k < response[index].trainingPhrases.length; k++) {
                            trainingPhrasesArr.push(response[index].trainingPhrases[k].parts[0].text)                            
                        }

                        returnedData.push({
                            displayName: response[index].displayName,
                            inputContextNames: inputArr,
                            trainingPhrases: trainingPhrasesArr,
                            outputContexts: outputArr,
                            messages: response[index].messages[0].text.text, // message for now, payload later
                            id_: _helper.getIdFromPath(response[index].name)
                        })
                    }  
                }

                res.send({
                    status: 'success',
                    information: returnedData
                })
            })
            .catch(function(err) {
                res.send({
                    status: 'fail',
                    infomation: err
                })
            })
})

module.exports = router