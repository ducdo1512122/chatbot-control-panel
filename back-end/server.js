const express = require('express')
require('dotenv').config()

// creating an express instance
const app = express()
const cookieSession = require('cookie-session')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const passport = require('passport')
const path = require('path')

// CORS middleware
const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', '*');
    next();
}

// Socket
const socket = require('./socket/socketServer')
const socketDialogflow = require('./socket/dialogflow/_session')

// Database
const database = require('./services/database/databaseServer')

// Router
const publicRouter = require('./routers/publicRouter/index')
const chatbotManagerRouter = require('./routers/chatbotManagerRouter/index')
const conversationRouter = require('./routers/conversationRouter/index')
const userManagerRouter = require('./routers/userManagerRouter/index')
const api = require('./routers/api/index')

// App set-up
app.use(passport.initialize())
app.use(express.static(path.join(__dirname, '/public/dist'), { dotfiles: 'allow' }))
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(allowCrossDomain)

app.use('/', publicRouter)
app.use('/conversation', conversationRouter)
app.use('/', chatbotManagerRouter)

socket.initServer()
socketDialogflow.getResponseFromQuery()

database.initServer()

app.listen(process.env.PORT, function() {
    console.log(`Listening to port ${process.env.PORT}`)
})