const chatbotModel = require("../../../database/chatbot/chatbot")

function getChatbot() {

}

function createChatbot(name, projectId) {
    const newChatbot = new chatbotModel({
        name: name,
        projectId: projectId
    })

    return newChatbot.save()
                    .then(function(newChatbot) {
                        return {
                            status: 'success',
                            information: newChatbot
                        }
                    })
                    .catch(function(err) {
                        return {
                            status: 'failed',
                            information: err
                        }
                    })
}

function updateChatbot() {

}

function deleteChatbot() {

}

function listChatbots() {
    return chatbotModel.find().select('name projectId -_id')
                        .then(function(chatbots) {
                            return {
                                status: 'success',
                                information: chatbots
                            }
                        })
                        .catch(function(err) {
                            return {
                                status: 'failed',
                                information: err
                            }
                        })
}

module.exports = {
    getChatbot,
    createChatbot,
    updateChatbot,
    deleteChatbot,
    listChatbots
}