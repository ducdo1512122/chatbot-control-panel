var mongoose = require('mongoose');

function initServer() {
    mongoose.connect(
        process.env.MONGO_PROTOCOL +
        process.env.HOST_NAME + ":" + process.env.MONGO_PORT +
        "/" + process.env.DATABASE,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }).catch(function(err) {
            console.log(err);
        });

    mongoose.connection.on('connected', err => {
        console.log(`Mongoose connection is opened at ${process.env.MONGO_PROTOCOL}${process.env.HOST_NAME}:${process.env.MONGO_PORT}/${process.env.DATABASE}`);
    });

    mongoose.connection.on('error', function(err){
        console.log("Mongoose connection has occured " + err + " error");
    });

    mongoose.connection.on('disconnected', function(){
        console.log("Mongoose default connection is disconnected");
    });
}

module.exports = {
    initServer,
}