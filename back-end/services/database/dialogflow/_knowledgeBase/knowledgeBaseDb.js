const knowledgeBaseModel = require("../../../../database/dialogflow/conversation/faq/knowledgeBase")

function createKnowledgeBase(name, displayName, languageCode, projectId, knowledgeBaseId) {
    let newKnowledgeBase = new knowledgeBaseModel({
        name: name,
        displayName: displayName,
        languageCode: languageCode,
        projectId: [projectId],
        knowledgeBaseId: knowledgeBaseId
    })

    newKnowledgeBase.save(function(err) {
        if(err) {
            console.log(err)
        }

        console.log("good")
    })
}

module.exports = {
    createKnowledgeBase
}