const dialogflow = require('dialogflow')
const authen = require('../config')

function contextContent(name, lifespanCount, parameters) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))
    const formattedPath = client.contextPath(projectId, sessionId, contextId)

    return {
        name: formattedPath,
        lifespanCount: "",
        parameters: ""
    }
}

async function getContextApi(projectId, sessionId, contextId) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))
    const formattedName = client.contextPath(projectId, sessionId, contextId)

    const request = {
        name: formattedName
    }

    const [response] = await client.getContext(request)
    return response
}

async function createContextApi(projectId, sessionId, context) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))
    const formattedParent = client.sessionPath(projectId, sessionId)

    const request = {
        parent: formattedParent,
        context: context
    }

    const [response] = await client.createContext(request)
    return response
}

async function updateContextApi(projectId, context) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))

    const request = {
        context: context
    }

    const [response] = await client.updateContext(request)
    return response
}

async function deleteContextApi(projectId, sessionId, contextId) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))
    const formattedName = client.contextPath(projectId, sessionId, contextId)

    const request = {
        name: formattedName
    }

    const [response] = await client.deleteContext(request)
    return response
}

async function deleteAllContextsApi(projectId, sessionId) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))
    const formattedParent = client.sessionPath(projectId, sessionId)

    const request = {
        parent: formattedParent
    }

    const [response] = await client.deleteAllContexts(request)
    return response
}

async function listContextsApi(projectId, sessionId) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))
    const formattedParent = client.sessionPath(projectId, sessionId)

    const request = {
        parent: formattedParent
    }

    const [response] = await client.listContexts(request)
    return response
}

module.exports = {
    contextContent,
    getContextApi,
    createContextApi,
    updateContextApi,
    deleteContext,
    deleteAllContexts,
    listContextsApi
}