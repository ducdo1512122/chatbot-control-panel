const dialogflow = require('dialogflow')
const authen = require('../config')

function entityContent() {

}

async function batchCreateEntitiesApi(projectId, entityType, entities) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.entityTypePath(projectId, entityType)

    const request = {
        parent: formattedParent,
        entities: entities,
    }

    const [operation] = await client.batchCreateEntities(request)
    const [response] = await operation.promise()
    return response
}

async function batchUpdateEntitiesApi(projectId, entityType, entities) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.entityTypePath(projectId, entityType)

    const request = {
        parent: formattedParent,
        entities: entities,
    }

    const [operation] = await client.batchUpdateEntities(request);
    const [response] = await operation.promise()
    return response
}

async function batchDeleteEntitiesApi(projectId, entityType, entities) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.entityTypePath(projectId, entityType)

    const request = {
        parent: formattedParent,
        entityValues: entities,
    }

    const [operation] = await client.batchDeleteEntities(request)
    const [response] = await operation.promise()
    return response
}

module.exports = {
    entityContent,
    batchCreateEntitiesApi,
    batchUpdateEntitiesApi,
    batchDeleteEntitiesApi
}