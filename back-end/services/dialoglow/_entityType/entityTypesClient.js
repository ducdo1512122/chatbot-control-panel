const dialogflow = require('dialogflow')
const authen = require('../config')

function entityTypeContent(projectId) {
    
}

async function getEntityTypeApi(projectId, entityTypeId) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedName = client.entityTypePath(projectId, entityTypeId)

    const request = {
        name: formattedName,
    }

    const [response] = await client.getEntityType(request)
    return response
}

async function createEntityTypeApi(projectId, entityType) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
        entityType: entityType,
    }

    const [response] = await client.createEntityType(request)
    return response
}

async function updateEntityTypeApi(projectId, entityType) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))

    const request = {
        entityType: entityType,
    }

    const [response] = await client.updateEntityType(request)
    return response
}

async function deleteEntityTypeApi(projectId, entityTypeId) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedName = client.entityTypePath(projectId, entityTypeId)

    const request = {
        name: formattedName,
    }

    const [response] = await client.deleteEntityType(request)
    return response
}

async function listEntityTypesApi(projectId) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
    }

    const [response] = await client.listEntityTypes(request)
    return response
}

async function batchUpdateEntityTypesApi(projectId) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
    }

    const [operation] = await client.batchDeleteEntityTypes(request)
    const [response] = await operation.promise()
    return response
}

/**
 * 
 * @param {String} projectId 
 * @param {Array} entityTypeNames 
 */
async function batchDeleteEntityTypesApi(projectId, entityTypeNames) {
    const client = new dialogflow.v2beta1.EntityTypesClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
        entityTypeNames: entityTypeNames,
    }

    const [operation] = await client.batchDeleteEntityTypes(request)
    const [response] = await operation.promise()
    return response
}

module.exports = {
    entityTypeContent,
    
    getEntityTypeApi,
    createEntityTypeApi,
    updateEntityTypeApi,
    deleteEntityTypeApi,
    listEntityTypesApi,

    batchUpdateEntityTypesApi,
    batchDeleteEntityTypesApi
}