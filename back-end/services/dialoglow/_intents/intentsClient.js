const dialogflow = require('dialogflow')
const authen = require('../config')

/**
 * 
 * @param {Array.<String>} inputContextNames Format: projects/<Project ID>/agent/sessions/-/contexts/<Context ID>
 */
function inputContextNamesStructure(projectId, sessionId, inputContextNames) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))

    let inputContextNamesArr = []

    for (let index = 0; index < inputContextNames.length; index++) {
        inputContextNamesArr.push(
            client.contextPath(projectId, sessionId, inputContextNames[index])
        )        
    }

    return inputContextNamesArr
}

/**
 * 
 * @param {Number} type use EXAMPLE: 1, following dialogflow docs
 * @param {Array.<String>} parts 
 */
function trainingPhrasesStructure(type, parts) {
    let partsArr = []
    for (let index = 0; index < parts.length; index++) {
        partsArr.push({
            type: type,
            parts: [
                {
                    text: parts[index]
                }
            ]
        })
    }
    return partsArr
}

function outputContextsStructure(projectId, sessionId, outputContexts, lifespanCount) {
    const client = new dialogflow.v2beta1.ContextsClient(authen.authentication(projectId))

    let outputContextsArr = []

    for (let index = 0; index < outputContexts.length; index++) {
        outputContextsArr.push({
            name: client.contextPath(projectId, sessionId, outputContexts[index]),
            lifespanCount: lifespanCount
        })        
    }

    return outputContextsArr
}

function messageStructure(messages) {
    let messagesArr = []

    for (let index = 0; index < messages.length; index++) {
        messagesArr.push(messages[index])        
    }

    return [{
        platform: 'PLATFORM_UNSPECIFIED',
        text: {
            text: messagesArr
        }
    }]
}

function intentContent(projectId, name, displayName, webhookState,
                    priority, isFallback, inputContextNames,
                    events, trainingPhrases, action,
                    outputContexts, resetContexts, parameters, messages) {
    if (name) { // use for update
        const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
        const formattedName = client.intentPath(projectId, name)

        return {
            name: formattedName,
            displayName: displayName,
            // webhookState: webhookState,
            // priority: priority,
            // isFallback: isFallback,
            inputContextNames: inputContextNames,
            // events: events,
            trainingPhrases: trainingPhrases,
            // action: action,
            outputContexts: outputContexts,
            // resetContexts: resetContexts,
            // parameters: parameters,
            messages: messages
        }
    }

    if (webhookState && priority && isFallback && events && action && resetContexts && parameters) {
        return { // use for create [displayName, inputContextNames,
                // trainingPhrases, outputContexts, messages]
            displayName: displayName,
            webhookState: webhookState,
            priority: priority,
            isFallback: isFallback,
            inputContextNames: inputContextNames,
            events: events,
            trainingPhrases: trainingPhrases,
            action: action,
            outputContexts: outputContexts,
            resetContexts: resetContexts,
            parameters: parameters,
            messages: messages
        }
    }
    

    return {
        displayName: displayName,
        inputContextNames: inputContextNames,
        trainingPhrases: trainingPhrases,
        outputContexts: outputContexts,
        messages: messages
    }
}

function batchIntentContent(projectId, sessionId, intents) {
    let formattedIntents = []

    for (let index = 0; index < intents.length; index++) {
        if (intents[index].name.length === 0) { // create
            let createdDisplayName = intents[index].displayName
            let createdInputContextNamesStructure = inputContextNamesStructure(projectId, sessionId, intents[index].inputContextNames)
            let createdTrainingPhrasesStructure = trainingPhrasesStructure(1, intents[index].trainingPhrases)
            let createdOutputContextsStructure = outputContextsStructure(projectId, sessionId, intents[index].outputContexts, 5)
            let createdMessagesStructure = messageStructure(intents[index].messages)

            let createdIntent = intentContent(null, null, createdDisplayName, null,
                                                null, null, createdInputContextNamesStructure,
                                                null, createdTrainingPhrasesStructure, null,
                                                createdOutputContextsStructure, null, null, createdMessagesStructure)
            
            formattedIntents.push(createdIntent)

        }
        else { // update
            let updatedInputContextNamesStructure = inputContextNamesStructure(projectId, sessionId, intents[index].inputContextNames)
            let updatedTrainingPhrasesStructure = trainingPhrasesStructure(1, intents[index].trainingPhrases)
            let updatedOutputContextsStructure = outputContextsStructure(projectId, sessionId, intents[index].outputContexts, 5)
            let updatedMessagesStructure = messageStructure(intents[index].messages)

            let updatedIntent = intentContent(projectId, intents[index].name, intents[index].displayName, null,
                                                null, null, updatedInputContextNamesStructure,
                                                null, updatedTrainingPhrasesStructure, null,
                                                updatedOutputContextsStructure, null, null, updatedMessagesStructure)
            
            formattedIntents.push(updatedIntent)
        }
    }
    return {
        intents: formattedIntents
    }
}

function batchDeletedIntent(projectId, intentName) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    let intentNameArr = []

    for (let index = 0; index < intentName.length; index++) {
        intentNameArr.push({
            name: client.intentPath(projectId, intentName[index])
        })
    }

    return intentNameArr
}

async function createIntentApi(projectId, intent) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
        intent: intent,
    }

    const [response] = await client.createIntent(request)
    return response
}

async function updateIntentApi(projectId, intent) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))

    const request = {
        intent: intent,
    }

    const [response] = await client.updateIntent(request)
    return response
}

async function getIntentApi(projectId, intentId) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    const formattedName = client.intentPath(projectId, intentId)

    const request = {
        name: formattedName,
        intentView: 'INTENT_VIEW_FULL'
    }

    const [response] = await client.getIntent(request)
    return response

}

async function deleteIntentApi(projectId, intentId) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    const formattedName = client.intentPath(projectId, intentId)

    const request = {
        name: formattedName
    }

    const [response] = await client.deleteIntent(request)
    return response
}

async function listIntentsApi(projectId) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
        intentView: 'INTENT_VIEW_FULL'
    }

    const [response] = await client.listIntents(request)
    return response
}

async function batchDeleteIntentsApi(projectId, intents) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
        intents: intents,
    }

    const [operation] = await client.batchDeleteIntents(request)
    const [response] = await operation.promise()
    return response
}

async function batchUpdateIntentsApi(projectId, batchIntents) {
    const client = new dialogflow.v2beta1.IntentsClient(authen.authentication(projectId))
    const formattedParent = client.projectAgentPath(projectId)

    const request = {
        parent: formattedParent,
        intentBatchInline: batchIntents
    }

    const [operation] = await client.batchUpdateIntents(request);
    const [response] = await operation.promise()
    return response
}

module.exports = {
    inputContextNamesStructure,
    trainingPhrasesStructure,
    outputContextsStructure,
    messageStructure,
    intentContent,
    batchIntentContent,
    batchDeletedIntent,

    getIntentApi,
    createIntentApi,
    updateIntentApi,
    deleteIntentApi,
    listIntentsApi,

    batchDeleteIntentsApi,
    batchUpdateIntentsApi
}