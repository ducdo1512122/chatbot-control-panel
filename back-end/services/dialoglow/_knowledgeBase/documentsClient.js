const dialogflow = require('dialogflow')
const authen = require('../config')

function documentContent(displayName, rawContent, mimeType = 'text/csv', knowledgeTypes = "FAQ", projectId = null, knowledgeBaseId = null, documentId = null) {
    if (projectId && knowledgeBaseId && documentId) { // use for update
        const client = new dialogflow.v2beta1.DocumentsClient(authen.authentication(projectId))
        const formattedName = client.documentPath(projectId, knowledgeBaseId, documentId)

        return {
            name: formattedName,
            displayName: displayName,
            mimeType: mimeType,
            knowledgeTypes: [knowledgeTypes],
            rawContent: rawContent,
            source: `rawContent`
        }
    }

    // use for create [displayName & rawContent]
    return {
        displayName: displayName,
        mimeType: mimeType,
        knowledgeTypes: [knowledgeTypes],
        rawContent: rawContent,
        source: `rawContent`
    }
}

/**
 * 
 * @param {projectId} projectId
 * @param {knowledgeBaseId} knowledgeBaseId 
 * @param {document content} document 
 */
async function createDocumentApi(projectId, knowledgeBaseId, document) {
    const client = new dialogflow.v2beta1.DocumentsClient(authen.authentication(projectId))
    const formattedParent = client.knowledgeBasePath(projectId, knowledgeBaseId)

    const request = {
        parent: formattedParent,
        document: document
    }

    const [operation] = await client.createDocument(request)
    const [response] = await operation.promise()
    return response
    
}

/**
 * 
 * @param {*} projectId 
 * @param {*} knowledgeBaseId 
 * @param {*} documentId 
 */
async function getDocumentApi(projectId, knowledgeBaseId, documentId) {
    const client = new dialogflow.v2beta1.DocumentsClient(authen.authentication(projectId))
    const formattedName = client.documentPath(projectId, knowledgeBaseId, documentId)

    const request = {
        name: formattedName
    }

    const [response] = await client.getDocument(request)
    return response
}

/**
 * 
 * @param {json} document 
 */
async function updateDocumentApi(document) {
    const client = new dialogflow.v2beta1.DocumentsClient(authen.authentication(projectId))

    const request = {
        document: document
    }

    const [operation] = await client.updateDocument(request)
    const [response] = await operation.promise()
    return response
}

/**
 * 
 * @param {*} projectId 
 * @param {*} knowledgeBaseId 
 * @param {*} documentId 
 */
async function deleteDocumentApi(projectId, knowledgeBaseId, documentId) {
    const client = new dialogflow.v2beta1.DocumentsClient(authen.authentication(projectId))
    const formattedName = client.documentPath(projectId, knowledgeBaseId, documentId)

    const request = {
        name: formattedName
    }

    const [operation] = await client.deleteDocument(request)
    const [response] = await operation.promise()
    return response
}

/**
 * 
 * @param {string} projectId 
 * @param {string} knowledgeBaseId 
 */
async function listDocumentsApi(projectId, knowledgeBaseId) {
    const client = new dialogflow.v2beta1.DocumentsClient(authen.authentication(projectId))
    const formattedParent = client.knowledgeBasePath(projectId, knowledgeBaseId)

    const request = {
        parent: formattedParent
    }

    const [response] = await client.listDocuments(request)
    return response
}

module.exports = {
    documentContent,
    createDocumentApi,
    getDocumentApi,
    updateDocumentApi,
    deleteDocumentApi,
    listDocumentsApi
}