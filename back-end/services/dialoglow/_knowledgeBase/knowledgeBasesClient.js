const dialogflow = require('dialogflow')
const authen = require('../config')
const _helper = require("../../../utils/dialogflow/_helper")

/**
 * 
 * @param {The project to create a knowledge base for} projectId 
 * @param {The knowledge base to create} knowledgeBaseName 
 */
async function createKnowledgeBaseApi(projectId, knowledgeBaseName) {
    const client = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))

    const formattedParent = client.projectPath(projectId)
    const knowledgeBase = {
        displayName: knowledgeBaseName
    }

    const request = {
        parent: formattedParent,
        knowledgeBase: knowledgeBase
    }

    const [result] = await client.createKnowledgeBase(request)
    return result
}

/**
 * 
 * @param {projectId} projectId 
 * @param {knowledgeBaseId} knowledgeBaseId 
 */
async function getKnowledgeBaseApi(projectId, knowledgeBaseId) {
    const client = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))
    
    // The name of the knowledge base to delete. Format: projects/<Project ID>/knowledgeBases/<Knowledge Base ID>
    const formattedName = client.knowledgeBasePath(projectId, knowledgeBaseId)

    const request = {
        name: formattedName
    }

    const [result] = await client.getKnowledgeBase(request)
    return result
}

/**
 * 
 * @param {The project to update a knowledge base for} projectId 
 * @param {The knowledge base to update} knowledgeBaseName 
 */
async function updateKnowledgeBaseApi(projectId, knowledgeBaseId, newDisplayName) {
    const client = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))

    const formattedName = client.knowledgeBasePath(projectId, knowledgeBaseId)

    const request = {
        name: formattedName,
        displayName: newDisplayName
    }

    const [result] = await client.updateKnowledgeBase(request)
    return result
}

/**
 * 
 * @param {projectId} projectId 
 * @param {knowledgeBaseId} knowledgeBaseId 
 */
async function deleteKnowledgeBaseApi(projectId, knowledgeBaseId) {
    const client = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))
    
    // The name of the knowledge base to delete. Format: projects/<Project ID>/knowledgeBases/<Knowledge Base ID>
    const formattedName = client.knowledgeBasePath(projectId, knowledgeBaseId)

    const request = {
        name: formattedName
    }

    const [result] = await client.deleteKnowledgeBase(request)
    return result
}

/**
 * 
 * @param {projectId} projectId 
 */
async function listKnowledgeBasesApi(projectId) {
    const client = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))
    
    // The name of the knowledge base to delete. Format: projects/<Project ID>
    const formattedParent = client.projectPath(projectId)

    const request = {
        parent: formattedParent
    }

    // Return array of knowledge base
    const [resources] = await client.listKnowledgeBases(request)
    return resources
}

module.exports = {
    createKnowledgeBaseApi,
    getKnowledgeBaseApi,
    updateKnowledgeBaseApi,
    deleteKnowledgeBaseApi,
    listKnowledgeBasesApi
}