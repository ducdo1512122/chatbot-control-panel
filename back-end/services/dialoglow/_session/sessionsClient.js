const dialogflow = require('dialogflow')
const authen = require('../config')

function queryContent(query, languageCode) {
    return {
        text: {
            text: query,
            languageCode: languageCode
        }
    }
}

async function detectIntentApi(projectId, sessionId, queryInput, contextInput) {
    const client = new dialogflow.v2beta1.SessionsClient(authen.authentication(projectId))
    const formattedSession = client.sessionPath(projectId, sessionId)

    const request = {
        session: formattedSession,
        queryInput: queryInput
    }

    if (contextInput && contextInput.length > 0) {
        Request.queryParams = {
            contexts: contextInput
        }
    }

    const responses = await client.detectIntent(request)
    return responses[0]
}

async function detectIntentWithKnowledgeBaseApi(projectId, sessionId, knowledgeBaseId, queryInput, contextInput) {
    const client = new dialogflow.v2beta1.SessionsClient(authen.authentication(projectId))
    const formattedSession = client.sessionPath(projectId, sessionId)

    const knowledgeBase = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))
    const knowledgeBasePath = knowledgeBase.knowledgeBasePath(projectId, knowledgeBaseId)

    const request = {
        session: formattedSession,
        queryInput: queryInput,
        queryParams: {
            knowledgeBaseNames: [knowledgeBasePath]
        }
    }

    const responses = await client.detectIntent(request)
    return responses[0]
}

async function detectIntentWithMultiKnowledgeBasesApi(projectId, sessionId, knowledgeBaseIdArray, queryInput, contextInput) {
    const client = new dialogflow.v2beta1.SessionsClient(authen.authentication(projectId))
    const formattedSession = client.sessionPath(projectId, sessionId)

    const knowledgeBase = new dialogflow.v2beta1.KnowledgeBasesClient(authen.authentication(projectId))
    const knowledgeBasePaths = []

    for (let index = 0; index < knowledgeBaseIdArray.length; index++) {
        knowledgeBasePaths.push(knowledgeBase.knowledgeBasePath(projectId, knowledgeBaseIdArray[index]))
        
    }

    const request = {
        session: formattedSession,
        queryInput: queryInput,
        queryParams: {
            knowledgeBaseNames: knowledgeBasePaths
        }
    }

    const responses = await client.detectIntent(request)
    return responses[0]
}

module.exports = {
    queryContent,
    detectIntentApi,
    detectIntentWithKnowledgeBaseApi,
    detectIntentWithMultiKnowledgeBasesApi
}