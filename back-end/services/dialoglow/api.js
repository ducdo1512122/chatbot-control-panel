/////////////////////////////////////////////////////////////////
// Chatbot actions
/////////////////////////////////////////////////////////////////
async function responseQuery(projectId, clientId, inputQuery, context = null, typeQuery) {

}

async function chatbotInformation(projectId) {

}

async function chatbotNavigation(projectId) { // Agent_Client
    // Show

    // Create

    // Update

    // Delele
}

async function conversationNavigation(projectId) { // Meta actions
    // Context, Intent, Entity, Session Entity

    // Show

    // Create

    // Update

    // Delele
}

/////////////////////////////////////////////////////////////////
// Knowledge Base
/////////////////////////////////////////////////////////////////
async function createFaq(projectId) { // create Knowledge Base

}

async function KnowledgeToFaqNavigation(projectId) { // update Document
    // Show

    // Create

    // Update

    // Delele
}

/////////////////////////////////////////////////////////////////
// External information (weather api, location api...)
/////////////////////////////////////////////////////////////////
async function getInformation(projectId, typeInfo) {

}

module.exports = {
    responseQuery,
    chatbotInformation,
    chatbotNavigation,
    conversationNavigation,
    //////////////////////
    createFaq,
    KnowledgeToFaqNavigation,
    //////////////////////
    getInformation
}