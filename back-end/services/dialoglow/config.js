const fs = require('fs');

function authentication (projectId) {
    let obj = JSON.parse(fs.readFileSync(process.env.CREDENTIAL_PATH + projectId + ".json", 'utf8'));

    let config = {
        credentials: {
            private_key: obj.private_key,
            client_email: obj.client_email
        }
    }
    
    return config
}

module.exports = {
    authentication
}