const server = require('../socketServer')
const io = require('socket.io')(server.socketServer)
const _session = require('../../services/dialoglow/_session/sessionsClient')

function getResponseFromQuery() {
    io.on('connection', function(socket) {
        socket.on('queryInput', function(data) {
            if (data.withKnowledgeBase) {

                let queryInput = _session.queryContent(data.queryInput, "en-US")
                _session.detectIntentWithMultiKnowledgeBasesApi(data.projectId, data.sessionId, data.knowledgeBaseId, queryInput, data.contextInput)
                    .then(function(response) {
                        if (response.queryResult.fulfillmentText.length == 0) {
                            socket.emit('responseOutput', response.queryResult.knowledgeAnswers.answers[0].answer)
                        }
                        else {
                            socket.emit('responseOutput', response.queryResult.fulfillmentText)
                        }
                        
                    })
            }
            else {
                _session.detectIntentApi(data.projectId, data.sessionId, data.queryInput, data.contextInput)
                    .then(function(response) {
                        socket.emit('responseOutput', response)
                    })
            }

            
        })
    })
}

module.exports = {
    getResponseFromQuery
}