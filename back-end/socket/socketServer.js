const express = require('express')
const app = express()
const socketServer = require('http').Server(app)

function initServer() {
    socketServer.listen(process.env.PORT_SOCKET, function() {
        console.log(`Port socket is ${process.env.PORT_SOCKET}`)
    })
}

module.exports = {
    initServer,
    socketServer
}