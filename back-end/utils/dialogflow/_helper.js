

function getIdFromPath(pathUri) {
    return pathUri.split("/").slice(-1)[0]
}

function buffer2string(jsonBuffer) {
    return jsonBuffer.toString("utf8")
}

module.exports = {
    getIdFromPath,
    buffer2string
}