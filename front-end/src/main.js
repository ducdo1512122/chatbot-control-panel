import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
// import store from './store'
import Axios from 'axios'
import Chat from 'vue-beautiful-chat'
import VueSocketIo from 'vue-socket.io'
import socketio from 'socket.io-client'

var ioInstance = socketio(process.env.VUE_APP_SOCKET)

Vue.use(Chat)
Vue.prototype.$http = Axios;
Vue.config.productionTip = false
const token = localStorage.getItem('token')
Vue.use(new VueSocketIo({
  debug: false,
  connection: ioInstance
}))

if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

new Vue({
  vuetify,
  router,
  // store,
  render: h => h(App)
}).$mount('#app')
