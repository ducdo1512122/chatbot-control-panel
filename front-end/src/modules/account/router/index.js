import LoginPage from '../LoginPage'

export default [
    {
        path: '/login',
        name: 'LoginPage',
        component: LoginPage
    }
]