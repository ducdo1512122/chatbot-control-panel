import ChatbotDashboard from '../ChatbotDashboard'
import ChatbotPopup from '../ChatTemplate/ChatbotPopup'

export default [
    {
        path: '/dashboard',
        name: 'ChatbotDashboard',
        component: ChatbotDashboard,
        // meta: { 
        //     requiresAuth: true
        // }
    },
    {
        path: '/demo',
        name: 'ChatbotPopup',
        component: ChatbotPopup
    }
]