import Vue from 'vue'
// import store from '../store/index.js'
import VueRouter from 'vue-router'
// import AccountRouter from '@/modules/account/router'
import ChatbotRouter from '@/modules/chatbot/router'
import MainPageRouter from '@/layouts/router'

Vue.use(VueRouter);

const baseRoutes = [];

const routes = baseRoutes.concat(MainPageRouter,/** AccountRouter, */  ChatbotRouter, { path: '*', redirect: '/'});

const router =  new VueRouter({
	mode: 'history',
	routes,
});

export default router;

// router.beforeEach((to, from, next) => {
// 	if (to.matched.some(record => record.meta.requiresAuth)) {
// 		if (store.getters.isLoggedIn) {
// 			next()
// 			return
// 		}
// 		next('/')
// 	}
// 	else {
//         next()
// 	}
// })