module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  devServer: {
    // READ DOCS: https://cli.vuejs.org/config/#devserver-proxy
    proxy: {
      '/conversation': {
        target: 'http://localhost:3000'
      }
    }
  }
}